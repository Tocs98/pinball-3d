﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getting_Ball_script : MonoBehaviour {

    public GameObject ball;
    public bool isRight;
    private Vector3 direction;
	// Use this for initialization
	void Start () {
        if (isRight)
        {
            direction = Vector3.right + (10 * Vector3.back);
        }
        else
        {
            direction = Vector3.left + (10 * Vector3.back);
        }
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Sphere")
        {
            col.GetComponent<Rigidbody>().isKinematic = true;
            Invoke("giveForce", 2f);
            GameObject points = GameObject.Find("Points Manager");
            points_manager pMan = points.GetComponent<points_manager>();
            pMan.points = pMan.points + 50;
        }
    }

    void giveForce()
    {
        ball.GetComponent<Rigidbody>().isKinematic = false;
        ball.GetComponent<Rigidbody>().AddForce(500 * direction);
    }

    // Update is called once per frame
    void Update () {
		
	}

}
