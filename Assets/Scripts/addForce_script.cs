﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addForce_script : MonoBehaviour {

    public float forceApplied;

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("Collision!");
        if (col.gameObject.name == "Sphere")
        {

            col.gameObject.GetComponent<Rigidbody>().AddForce(0, 0, Random.Range(-300f, 3500f));
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
