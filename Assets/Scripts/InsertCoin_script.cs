﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsertCoin_script : MonoBehaviour {
    public Animation anim;
    private bool gameHasStarted;
    private GameObject[] text;
    private GameObject press;
    private GameObject ball;
    // Use this for initialization
    void Start () {
        gameHasStarted = false;
        text = GameObject.FindGameObjectsWithTag("texts");
        press = GameObject.FindGameObjectWithTag("press");
        ball = GameObject.FindGameObjectWithTag("Ball");

        ball.active = false;
        for (int i = 0; i < text.Length; i++)
        {
            text[i].active = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
        {
            if (!gameHasStarted)
            {
                gameHasStarted = true;
                Invoke("insertCoin", 0.1f);
            }
            
        }
	}

    public void insertCoin()
    {
        anim.Play();
        for (int i = 0; i < text.Length; i++)
        {
            text[i].active = true;
        }
        press.SetActive(false);
        ball.active = true;
    }
}
