﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ouPinball_script : MonoBehaviour {

    public GameObject ball;

    private float restX;
    private float restY;
    private float restZ;

    public Text livesScore;

    private int lives;


    void Start()
    {
        restX = ball.transform.position.x;
        restY = ball.transform.position.y;
        restZ = ball.transform.position.z;

        lives = 3;

        livesScore.text = "Lives: " + lives.ToString();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Sphere")
        {
            lives--;
            livesScore.text = "Lives: " + lives.ToString();
            if (lives <= 0)
            {
                /*GameObject points = GameObject.Find("Points Manager");
                points_manager pMan = points.GetComponent<points_manager>();
                pMan.gameOver = true;*/
                livesScore.text = "GAME OVER!";
                Invoke("restartGame", 10f);
            }
            else
            {
                ball.transform.position = transform.rotation * (new Vector3(restX, restY, restZ));
            }
        }
    }

    private void restartGame()
    {
        SceneManager.LoadScene("MainScene");
    }



}
