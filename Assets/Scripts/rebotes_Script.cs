﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rebotes_Script : MonoBehaviour {
    public GameObject lights;

	// Use this for initialization
	void Start () {       
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    private void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.name == "Sphere")
        {
            lights.SetActive(true);
            this.gameObject.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);

            Invoke("setFalse", 2.0f);  //calling/Invoking the function after 2 seconds.

            GameObject points = GameObject.Find("Points Manager");
            points_manager pMan = points.GetComponent<points_manager>();
            pMan.points = pMan.points + 10;
        }
    }

    void setFalse()
    {
        this.gameObject.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        lights.SetActive(false);
    }
}
