﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class add_force_blocker_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Sphere")
        {
            col.rigidbody.AddForce(1000 * Vector3.back);
        }
    }
}
