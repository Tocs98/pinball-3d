﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audio_script : MonoBehaviour {

    public AudioClip[] audio;
    private AudioSource source;

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Flippers")
        {
            source.clip = audio[4];
            source.Play();
        }
        if (col.gameObject.tag == "Triangle")
        {
            source.clip = audio[3];
            source.Play();
        }
        if (col.gameObject.tag == "Points")
        {
            source.clip = audio[3];
            source.Play();
        }
    }
}
