﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blocker_Script : MonoBehaviour {

    private float startX;
    private float startY;
    private float startZ;
    public bool hasToGoUp;
    private bool oneTimeHit;
    public float timer;

    // Use this for initialization
    void Start () {
        startX = transform.position.x;
        startY = transform.position.y;
        startZ = transform.position.z;
        oneTimeHit = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Sphere")
        {
            Invoke("putDown", 1.0f);
        }
    }

    void putDown()
    {
        gameObject.transform.localPosition = new Vector3(-3.64f, 2.485f, 5.303f);
        GameObject upMan = GameObject.Find("ManagerBlocker");
        blocker_Manager bMan = upMan.GetComponent<blocker_Manager>();
        if (hasToGoUp)
        {
            if (!oneTimeHit)
            {
                bMan.counts += 1;
                oneTimeHit = true;
            }            
        }
        if (!hasToGoUp)
        {
            Invoke("returnPos", timer);
        }
        
    }

    public void return2Pos()
    {
        gameObject.transform.localPosition = new Vector3(startX, startY, startZ);
        oneTimeHit = false;
    }

    void returnPos()
    {
        gameObject.transform.localPosition = new Vector3(startX, startY, startZ);
    }
}
