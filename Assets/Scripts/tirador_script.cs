﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tirador_script : MonoBehaviour
{
    float power = 0f;
    public float powerAdd = 1000f;
    public float maxPower = 1000f;
    public Slider powerSlider;

    Rigidbody ball;

    void Start()
    {
        powerSlider.minValue = 0f;
        powerSlider.maxValue = maxPower;
    }

    private void FixedUpdate()
    {
        powerSlider.value = power;
        if (ball != null)
        {
            if (Input.GetAxis("Shoot") == 1)
            {
                if (power <= maxPower)
                {
                    power += powerAdd * Time.deltaTime;
                }
            }
            else
            {
                ball.AddForce(power * Vector3.back);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            powerSlider.gameObject.SetActive(true);
            ball = other.gameObject.GetComponent<Rigidbody>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            powerSlider.gameObject.SetActive(false);
            ball = null;
            power = 0f;
        }
    }
}

