﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class points_manager : MonoBehaviour {

    public int points;
    public Text text;
    public bool gameOver;

	// Use this for initialization
	void Start () {
        points = 0;
        text.text = "Score: " + points.ToString();
        gameOver = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (!gameOver)
        {
            text.text = "Score: " + points.ToString();
        }
        else
        {
            text.text = "GAME OVER!";
        }
       
    }

}
